# Multiple-collisions Smoluchowski-type aggregation solvers
Based on Discrete Convolution/ Discrete Fast Fourier Transform implementations
(Intel� MKL/VSL)
================================================================================

```
libcp   	--- library for sequential implementation of many-particle Smoluchowski aggregation operator
                using predefined CPD to represent kinetic coefficients  
libtt   	---	library for sequential and mpi-parallel implementation of many-particle Smoluchowski
                aggregation operator using TT-approximation to represent kinetic coefficients  
solvers 	--- sequential and mpi-parallel numerical solvers for Cauchy problem for many-particle
                aggregation
```
================================================================================