#include <omp.h>
#include <cstring>
#include <cmath>
#include <sys/time.h>

#include "gsmol_kernel.h"
#include "gsmol_tt_kernel.h"

//============================================================================//
//    Kernels initialization                                                  //
//============================================================================//
TKernel_TT::TKernel_TT(
    const char * kname,
    // in: type of collisions
    const char * ktype,
    // in: kernels maximal dimension
    const int & dim,
    // in: quantity of equations
    const int & mode,
    // in: weights in front of kernels
    const double * w,
    // in: tolerance of approximation
    const double & tol
):
    D(dim),
    A((!strcmp(ktype, "all"))? dim - 1: 1),
    N(mode),
    R(0)
{
    int * modes_sizes[A];

    for (int a = 0; a < A; ++a)
    {
        modes_sizes[a] = (int *)malloc(D * sizeof(int));

#pragma omp parallel for 
        for (int d = 0; d < D; ++d)
            modes_sizes[a][d] = N;
    }

    // multipliers in front of kernels initialization
    mult = (double *)calloc(A, sizeof(double));

    double factor = 0.5;

    if (A == 1)
    {
        mult[0] = w[0] / (double)fact(D);
    }
    else
    {
        mult[0] = factor * w[0];
    }

#pragma omp parallel for 
    for (int a = 1; a < A; ++a)
    {
        factor /= (double)(a + 2);
        mult[a] = factor * w[a];
    }

    // kernels initialization
    TKernel * kers[A];

    kers[A - 1] = new TKernel(D, modes_sizes[A - 1], kname);

#pragma omp parallel for 
    for (int a = 0; a < A - 1; ++a)
        kers[a] = new TKernel(a + 2, modes_sizes[a], kname);

    kers_tt = (TTensorTrain **)malloc(A * sizeof(TTensorTrain *));

#pragma omp parallel for 
    for (int a = 0; a < A; ++a)
        kers_tt[a] = new TTensorTrain();

    // parameters structure for TT Cross approximation 
    TTensorTrainParameters parameters;

    // convergence parameter
    parameters.tolerance = tol;
    // number of iterations. iterates until convergence if set as 0
    parameters.maximal_iterations_number = 0;

    //========================//
    //    TT-approximation    //
    //========================//
    for (int a = 0; a < A; ++a)
    {
        t_appr = omp_get_wtime();
        (kers_tt[a])->Approximate(kers[a], parameters);
        t_appr = omp_get_wtime() - t_appr;

        printf(
            "Approximation finished N = %d, d = %d, t = %.2f\n",
            N, a + 2, t_appr
        );
    }
    
    ranks_appr = (int *)malloc((D + 1) * sizeof(int)); 

#pragma omp parallel for 
    for (int d = 0; d <= D; ++d)
        ranks_appr[d] = (kers_tt[A - 1])->get_rank(d);

    //=======================//
    //    SVD-compression    //
    //=======================//
    t_compress = 0;
    // for (int a = 0; a < A; ++a)
    // {
    //     t_compress = clock();
    //     kers_tt[a].SVDCompress(parameters.tolerance / 1.);
    //     t_compress = clock() - t_compress;
    // }
    
    for (int a = 0; a < A; ++a)
        for (int d = 1; d < (kers_tt[a])->get_dimensionality(); ++d)
            if ((kers_tt[a])->get_rank(d) > R)
                R = (kers_tt[a])->get_rank(d);

    aux = (double *)malloc((((R << 1) + 1) * R + 1) * N * sizeof(double));

    tasks = (VSLConvTaskPtr *)malloc(R * R * sizeof(VSLConvTaskPtr));

    vsldConvNewTask1D(tasks, VSL_CONV_MODE_AUTO, N, N, N);

#pragma omp parallel for
    for (int r = 1; r < R * R; ++r)
    {
        vslConvCopyTask(tasks + r, tasks[0]);
    }

#pragma omp parallel for 
    for (int a = 0; a < A; ++a)
        delete kers[a];

    return;
}

//============================================================================//
//    Kernels deallocation                                                    //
//============================================================================//
TKernel_TT::~TKernel_TT(
) {
#pragma omp parallel for 
    for (int a = 0; a < A; ++a)
        delete kers_tt[a];

    if (kers_tt)
        free(kers_tt);

#pragma omp parallel for
    for (int r = 0; r < R * R; ++r)
        vslConvDeleteTask(tasks + r);

    if (aux)
        free(aux);

    if (ranks_appr)
        free(ranks_appr);

    if (mult)
        free(mult);

    return;
}

//============================================================================//
//    Income part of Smoluchowski operator                                    //
//============================================================================//
void TKernel_TT::gconv(
    // in: index of kernel to apply
    const int & kind,
    // in:
    const double * arg,
    // out: income part of S^(kind)(arg)
    double * res
) {
    const int d = (kers_tt[kind])->dimensionality;
    int * ranks = (kers_tt[kind])->ranks;
    double ** carriages = (kers_tt[kind])->carriages;

    int status;
    const double dzero = 0.;
    double dones[R];

    dones[0] = 1.;
    copy(R, dones, 0, dones, 1);

    // aux ~ R * R * N
    // lucar ~ R * R * N
    // rucar ~ R * N
    double * lucar = aux + R * R * N;
    double * rucar = lucar + R * R * N;

    // res = 0
    copy(N, &dzero, 0, res, 1); 

    //============================//
    //    Last carriage update    //
    //============================//
    // rucar[rr] = carriages[d - 1][rr] x arg
#pragma omp parallel for collapse(2)
    for (int i = 0; i < N; ++i)
    {
        for (int rr = 0; rr < ranks[d - 1]; ++rr)
        {
            rucar[i + rr * N] = carriages[d - 1][rr + i * ranks[d - 1]] * arg[i];
        }
    }

    for (int a = d - 1; a > 0; --a)
    {
        //=================================//
        //    Next left carriage update    //
        //=================================//
        // lucar[lr][rr] = carriages[a - 1][lr][rr] x arg
#pragma omp parallel for collapse(3)
        for (int i = 0; i < N; ++i)
        {
            for (int rr = 0; rr < ranks[a]; ++rr)
            {
                for (int lr = 0; lr < ranks[a - 1]; ++lr)
                {
                    lucar[i + (lr + rr * R) * N] = carriages[a - 1][
                        rr + ranks[a] * lr + i * (ranks[a - 1] * ranks[a])
                    ] * arg[i];
                }
            }
        }

        //==============================================//
        //    Convolution along right rank dimension    //
        //==============================================//
        // aux[lr][rr] = lucar[lr][rr] * rucar[rr]
#pragma omp parallel for collapse(2)
        for (int lr = 0; lr < ranks[a - 1]; ++lr)
        {
            for (int rr = 0; rr < ranks[a]; ++rr)
            {
                status = vsldConvExec1D(
                    tasks[lr + rr * R],
                    lucar + (lr + rr * R) * N, 1,
                    rucar + rr * N, 1,
                    aux + (lr + rr * R) * N, 1
                );
            }
        }

        //================================================//
        //    Sum of convolutions along mode dimension    //
        //================================================//
        // rucar = aux · [1]
        gemv(
            CblasColMajor, CblasNoTrans,
            ranks[a - 1] * N, ranks[a],
            dones[0], aux, R * N,
            dones, 1,
            dzero, rucar, 1
        );
    }

    // res = rucar >> d - 1
    copy(N - d + 1, rucar, 1, res + d - 1, 1);

    return;
}

//============================================================================//
//    Outcome part of Smoluchowski operator                                   //
//============================================================================//
void TKernel_TT::gmatvec(
    // in: index of kernel to apply
    const int & kind,
    // in:
    const double * arg,
    // out: outcome part of S^(kind)(arg)
    double * res
) {
    const int d = (kers_tt[kind])->dimensionality;
    int * ranks = (kers_tt[kind])->ranks;
    double ** carriages = (kers_tt[kind])->carriages;

    const double dzero = 0.;
    double dones[N];

    dones[0] = 1.;
    copy(N, dones, 0, dones, 1);

    // aux ~ R * R
    // lucar ~ R * R * N
    // rucar ~ R * N
    double * lucar = aux + R * R;
    double * rucar = lucar + R * R * N;

    //============================//
    //    Last carriage update    //
    //============================//
    // rucar[rr] = carriages[d - 1][rr] x arg
#pragma omp parallel for collapse(2)
    for (int i = 0; i < N; ++i)
    {
        for (int rr = 0; rr < ranks[d - 1]; ++rr)
        {
            rucar[i + rr * N] = carriages[d - 1][rr + i * ranks[d - 1]] * arg[i];
        }
    }

    for (int a = d - 1; a > 0; --a)
    {
        //=================================//
        //    Next left carriage update    //
        //=================================//
        // lucar[lr][rr] = carriages[a - 1][lr][rr] x arg
#pragma omp parallel for collapse(3)
        for (int i = 0; i < N; ++i)
        {
            for (int rr = 0; rr < ranks[a]; ++rr)
            {
                for (int lr = 0; lr < ranks[a - 1]; ++lr)
                {
                    lucar[(i * R + lr) * R + rr]
                        = carriages[a - 1][
                            rr + ranks[a] * lr + i * (ranks[a - 1] * ranks[a])
                        ] * arg[i];
                }
            }
        }

        //=================================================//
        //    Sum of left carriage along mode dimension    //
        //=================================================//
        // aux = lucar · [1]
        gemv(
            CblasColMajor, CblasNoTrans,
            R * R, N, dones[0],
            lucar, R * R,
            dones, 1,
            dzero, aux, 1
        );
        
        //==========================================//
        //    Product along right rank dimension    //
        //==========================================//
        // lucar = rucar
        copy(R * N, rucar, 1, lucar, 1);

        // rucar = aux · lucar
        gemm(
            CblasColMajor, CblasNoTrans, CblasNoTrans,
            N, ranks[a - 1], ranks[a], dones[0],
            lucar, N,
            aux, R,
            dzero, rucar, N
        );
    }

    // res = rucar
    copy(N, rucar, 1, res, 1);

    return;
}

//============================================================================//
//    Smoluchowski operator                                                   //
//============================================================================//
void TKernel_TT::compute(
    // in:
    const double * arg,
    // out: S(arg)
    double * res
) {
    double * tmp = aux + ((R << 1) + 1) * R * N;

    if (A == 1)
    {
        this->gconv(0, arg, res);
        this->gmatvec(0, arg, tmp);
        // this->gmatvec(0, arg, res);

        axpy(N, -D, tmp, 1, res, 1);
        scal(N, mult[0], res, 1);
    }
    else
    {
        this->gconv(0, arg, res);
        this->gmatvec(0, arg, tmp);

        axpy(N, -2., tmp, 1, res, 1);
        scal(N, mult[0], res, 1);

        for (int a = 1; a < A; ++a)
        {
            this->gconv(a, arg, tmp);
            axpy(N, mult[a], tmp, 1, res, 1);

            this->gmatvec(a, arg, tmp);
            axpy(N, -(double)(a + 2) * mult[a], tmp, 1, res, 1);
        }
    }

    return;
}
