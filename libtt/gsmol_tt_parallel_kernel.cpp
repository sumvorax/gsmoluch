#include <omp.h>
#include <stddef.h>
#include <cstring>
#include <cmath>

#include "gsmol_kernel.h"
#include "gsmol_tt_parallel_kernel.h"
// #define MKL_Complex16 std::complex<double>

//============================================================================//
//    Default constructor                                                     //
//============================================================================//
TKernel_TT::TKernel_TT(
    void
):
    D(-1),
    A(-1),
    N(-1),
    R(-1),
    L(-1),
    mult(NULL),
    kers_tt(NULL),
    psize(-1),
    prank(-1),
    loads(NULL),
    offs(NULL),
    lchunks(NULL),
    spans(NULL),
    rchunks(NULL),
    mchunk(-1),
    mld(-1),
    moff(-1),
    H(-1),
    comms(NULL),
    descs(NULL),
    dones(NULL),
    aux(NULL),
    faux(NULL),
    ranks_appr(NULL),
    t_appr(-1),
    t_compress(-1),
    t_transfer(-1)
{}

//============================================================================//
//    Kernels initialization                                                  //
//============================================================================//
TKernel_TT::TKernel_TT(
    const char * kname,
    // in: type of collisions
    const char * ktype,
    // in: kernels maximal dimension
    const int & kdim,
    // in: quantity of equations
    const int & kmode,
    // in: weights in front of kernels
    const double * kw,
    // in: tolerance of approximation
    const double & ktol,
    // in: MPI thread support
    const int & support
):
    D(kdim),
    A((!strcmp(ktype, "all"))? kdim - 1: 1),
    N(kmode),
    R(1),
    L(1),
    t_transfer(0)
{
    MPI_Comm_size(MPI_COMM_WORLD, &psize);
    MPI_Comm_rank(MPI_COMM_WORLD, &prank);

    kers_tt = (TTensorTrain **)malloc(A * sizeof(TTensorTrain *));

    loads = (int **)malloc(A * sizeof(int *));
    offs = (int **)malloc(A * sizeof(int *));
    lchunks = (int **)malloc(A * sizeof(int *));
    spans = (int **)malloc(A * sizeof(int *));
    rchunks = (int **)malloc(A * sizeof(int *));

    ranks_appr = (int *)malloc((D + 1) * sizeof(int)); 

    mult = (double *)calloc(A, sizeof(double));

    // initialize kernels multipliers 
    double factor = 0.5;

    mult[0] = (A == 1)? kw[0] / (double)fact(D): factor * kw[0];
   
    for (int a = 1; a < A; ++a)
    {
        factor /= (double)(a + 2);
        mult[a] = factor * kw[a];
    }

    // temporary 
    int dim;
    int * ranks = NULL;
    double ** carriages = NULL;

    //=================================//
    //    TT-kernels initialization    //
    //=================================//
    for (int a = 0; a < A; ++a)
    {
        dim = (a < A - 1)? a + 2: D;

        kers_tt[a] = new TTensorTrain();

        // allocate navigational arrays
        loads[a] = (int *)malloc((dim - 1) * sizeof(int));
        offs[a] = (int *)malloc((dim - 1) * sizeof(int)); 
        lchunks[a] = (int *)malloc((dim - 1) * sizeof(int));
        spans[a] = (int *)malloc((dim - 1) * sizeof(int)); 
        rchunks[a] = (int *)malloc((dim - 1) * sizeof(int));

        //================================//
        //    TT-kernels approximation    //
        //================================//
        if (!prank)
        {
            int * modes = (int *)malloc(dim * sizeof(int));

            // initialize kernel modes
            modes[0] = N;
            copy(dim, modes, 0, modes, 1); 

            // initialize kernel 
            TKernel * ker = new TKernel(dim, modes, kname);

            // initialize parameters for TT-cross approximation 
            TTensorTrainParameters pars;

            // convergence tolerance
            pars.tolerance = ktol;
            // number of iterations
            // if zero then iterates until convergence
            pars.maximal_iterations_number = 0;

            // TT-cross approximation
            t_appr = MPI_Wtime();
            (kers_tt[a])->Approximate(ker, pars);
            t_appr = MPI_Wtime() - t_appr;

            printf(
                "Approximation finished p = %d, N = %d, d = %d, t = %.2f\n",
                psize, N, dim, t_appr
            );

            // save approximation ranks of the highest TT-kernel
            if (a == A - 1)
            {
                copy(D + 1, (kers_tt[A - 1])->ranks, 1, ranks_appr, 1);
            }

            // SVD-compression
            // unused for now
            t_compress = 0;
            // t_compress = clock();
            // (kers_tt[a])->SVDCompress(parameters.tolerance / 1.);
            // t_compress = clock() - t_compress;

            delete ker;
        }
        else
        // allocate TT-kernel on non-root processors
        {
            (kers_tt[a])->modes_sizes = (int *)malloc(dim * sizeof(int));
            (kers_tt[a])->ranks = (int *)malloc((dim + 1) * sizeof(int));
            (kers_tt[a])->carriages = (double **)malloc(dim * sizeof(double *));

            // initialize kernel modes
            (kers_tt[a])->modes_sizes[0] = N;
            copy(
                dim, (kers_tt[a])->modes_sizes, 0, (kers_tt[a])->modes_sizes, 1
            ); 
        }

        // broadcast dimensionality
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Bcast(
            &((kers_tt[a])->dimensionality), 1, MPI_INT, 0, MPI_COMM_WORLD
        );

        // broadcast ranks 
        MPI_Bcast(
            (kers_tt[a])->ranks, dim + 1, MPI_INT, 0, MPI_COMM_WORLD
        );

        ranks = (kers_tt[a])->ranks;
        carriages = (kers_tt[a])->carriages;

        //==============================//
        //    Carriages distribution    //
        //==============================//
        for (int d = 0; d < dim - 1; ++d)
        {
            // initialize navigational arrays
            loads[a][d] = ranks[d] * ranks[d + 1];
            offs[a][d] = 0;
            lchunks[a][d] = 0;
            spans[a][d] = ranks[d];
            rchunks[a][d] = 0;

            // update max rank
            if (R < ranks[d + 1])
            {
                R = ranks[d + 1];
            }

            // update max load
            if (L < loads[a][d])
            {
                L = loads[a][d];
            }

            if (prank)
            {
                carriages[d] = (double *)malloc(
                    N * loads[a][d] * sizeof(double)
                );
            }

            if (psize > 1)
            {
                MPI_Barrier(MPI_COMM_WORLD);
                MPI_Bcast(
                    carriages[d], N * loads[a][d], MPI_DOUBLE, 0, MPI_COMM_WORLD
                );
            }
        }

        // allocate last carriage on non-root
        if (prank)
        {
            carriages[dim - 1] = (double *)malloc(
                N * ranks[dim - 1] * sizeof(double)
            );
        }

        // broadcast last carriage
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Bcast(
            carriages[dim - 1], N * ranks[dim - 1],
            MPI_DOUBLE, 0, MPI_COMM_WORLD
        );

        MPI_Barrier(MPI_COMM_WORLD);
  }
  
    // broadcast approximation ranks of the highest TT-kernel 
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(
        ranks_appr, D + 1, MPI_INT, 0, MPI_COMM_WORLD
    );

    // allocate auxiliary memory
    aux = (double *)malloc(((((L + R) << 1) + 1) * N + L) * sizeof(double));

    dones = (double *)malloc(((R < N)? N: R) * sizeof(double));
    dones[0] = 1.;
    copy(((R < N)? N: R), dones, 0, dones, 1);

    H = omp_get_max_threads();
    /// debug ///H = 1;
    H = (support == MPI_THREAD_MULTIPLE)? ((L < H)? L: H): 1;

    comms = (MPI_Comm *)malloc(H * sizeof(MPI_Comm));

    for (int h = 0; h < H; ++h)
    {
        MPI_Comm_dup(MPI_COMM_WORLD, comms + h);
    }

    descs = (DFTI_DESCRIPTOR_DM_HANDLE *)malloc(
        H * sizeof(DFTI_DESCRIPTOR_DM_HANDLE)
    );

#pragma omp parallel for
    for (int h = 0; h < H; ++h)
    {
        if (h != omp_get_thread_num())
        {
            if (!prank)
            {
                printf("ERROR: TT-kernel constructor:"
                    " Thread to CDFT descriptor mismatch\n");
                fflush(stdout);
            }

            MPI_Abort(MPI_COMM_WORLD, 2);
        }

        MKL_LONG status;

        status = DftiCreateDescriptorDM(
            comms[h], descs + h, DFTI_DOUBLE, DFTI_COMPLEX, 1, N
        );

        status = DftiSetValueDM(
            descs[h], DFTI_BACKWARD_SCALE, 0.5 / (double)N
        );
        status = DftiSetValueDM(descs[h], DFTI_PLACEMENT, DFTI_NOT_INPLACE);
        status = DftiSetValueDM(
            descs[h], DFTI_ORDERING, DFTI_BACKWARD_SCRAMBLED
        );

        status = DftiCommitDescriptorDM(descs[h]);
    }

    MKL_LONG status;

    status = DftiGetValueDM(descs[0], CDFT_LOCAL_X_START, &moff);
    status = DftiGetValueDM(descs[0], CDFT_LOCAL_SIZE, &mld);
    status = DftiGetValueDM(descs[0], CDFT_LOCAL_NX, &mchunk);

    faux = (std::complex<double> *)mkl_malloc(
        3 * H * mld * sizeof(std::complex<double>), 64
    );

#pragma omp parallel for
    for (int h = 0; h < H; ++h)
    {
        MKL_LONG status;

        MPI_Barrier(MPI_COMM_WORLD);
        status = DftiComputeForwardDM(
            descs[h], faux + (h << 1) * mld, faux + ((h << 1) + 1) * mld
        );
        status = DftiComputeBackwardDM(
            descs[h], faux + (h << 1) * mld, faux + ((h << 1) + 1) * mld
        );
    }

    return;
}

//============================================================================//
//    Kernels deallocation                                                    //
//============================================================================//
TKernel_TT::~TKernel_TT(
    void
)
{
    if (mult)
        free(mult);

#pragma omp parallel for 
    for (int a = 0; a < A; ++a)
    {
        delete kers_tt[a];

        free(loads[a]);
        free(offs[a]);
        free(lchunks[a]);
        free(spans[a]);
        free(rchunks[a]);
    }

    if (kers_tt)
        free(kers_tt);

    if (loads)
        free(loads);

    if (offs)
        free(offs);

    if (lchunks)
        free(lchunks);

    if (spans)
        free(spans);

    if (rchunks)
        free(rchunks);

#pragma omp parallel for
    for (int h = 0; h < H; ++h)
    {
        DftiFreeDescriptorDM(descs + h);
        MPI_Comm_free(comms + h);
    }

    if (comms)
        free(comms);

    if (descs)
        free(descs);

    if (dones)
        free(dones);

    if (aux)
        free(aux);

    if (faux)
        mkl_free(faux);

    if (ranks_appr)
        free(ranks_appr);

    return;
}

//============================================================================//
//    Binary convolution using +1 and -1 circulant                            //
//============================================================================//
void TKernel_TT::conv(
    // in: index of CDFT descriptor
    const int & h,
    // in: arg1
    const double * arg1,
    // in: arg2
    const double * arg2,
    // out: arg1 * arg2
    double * res
)
{
    const double dzero = 0.;
    int status;
    std::complex<double> * farg1 = faux + 3 * h * mld;
    std::complex<double> * farg2 = farg1 + mld;
    std::complex<double> * fimg = farg2 + mld;

    //====================//
    //    +1-circulant    //
    //====================//
    copy(mchunk, arg1, 1, (double *)farg1, 2);
    copy(mchunk, &dzero, 0, ((double *)farg1) + 1, 2);
    copy(mchunk, arg2, 1, (double *)farg2, 2);
    copy(mchunk, &dzero, 0, ((double *)farg2) + 1, 2);

    status = DftiComputeForwardDM(descs[h], farg1, fimg);
    status = DftiComputeForwardDM(descs[h], farg2, farg1);

//#pragma omp parallel for
    for (int i = 0; i < mld; ++i)
    {
        fimg[i] *= farg1[i];
    }

    status = DftiComputeBackwardDM(descs[h], fimg, farg1);
    copy(mchunk, (double *)farg1, 2, res, 1);

    //====================//
    //    -1-circulant    //
    //====================//
//#pragma omp parallel for
    for (int i = 0; i < mchunk; ++i)
    {
        ((double *)farg1)[2 * i]
            = arg1[i] * cos(M_PI * (moff + i) / (double)N);
        ((double *)farg1)[2 * i + 1]
            = -arg1[i] * sin(M_PI * (moff + i) / (double)N);

        ((double *)farg2)[2 * i]
            = arg2[i] * cos(M_PI * (moff + i) / (double)N);
        ((double *)farg2)[2 * i + 1]
            = -arg2[i] * sin(M_PI * (moff + i) / (double)N);
    }

    status = DftiComputeForwardDM(descs[h], farg1, fimg);
    status = DftiComputeForwardDM(descs[h], farg2, farg1);

//#pragma omp parallel for
    for (int i = 0; i < mld; ++i)
    {
        fimg[i] *= farg1[i];
    }

    status = DftiComputeBackwardDM(descs[h], fimg, farg1);

//#pragma omp parallel for
    for (int i = 0; i < mchunk; ++i)
    {
        res[i] += cos(M_PI * (moff + i) / (double)N) * ((double *)farg1)[2 * i]
            - sin(M_PI * (moff + i) / (double)N) * ((double *)farg1)[2 * i + 1];
    }

    return;
}

//============================================================================//
//    Smoluchowski operator for a-th kernel                                   //
//============================================================================//
void TKernel_TT::gsmol(
    // in: index of kernel to apply
    const int & a,
    // in:
    const double * arg,
    // out: S^(a)(arg)
    double * res
) {
    const int dim = (kers_tt[a])->dimensionality;
    int * ranks = (kers_tt[a])->ranks;
    double ** carriages = (kers_tt[a])->carriages;

    int is_lchunk;
    int is_rchunk;
    const double dzero = 0.;

    // aux ~ L
    // conv ~ L * N
    // lucar ~ L * N
    // rucarc & rucarp ~ 2 * R * N
    double * conv = aux + L;
    double * lucar = conv + L * mchunk;
    double * rucarc = lucar + L * mchunk;
    double * rucarp = rucarc + ranks[dim - 2] * mchunk;

    // where to keep current matvec-updated right factor  
    double * tmp = rucarc;

    // printf("prank = %d, mchunk = %d, mld = %d\n", prank, mchunk, mld);
    // fflush(stdout);
    //====================================//
    //    Last carriage partial update    //
    //====================================//
    // tmp[rr] = carriages[dim - 1][rr] x arg
#pragma omp parallel for collapse(2)
    for (
        int r = 0;
        r < (
            (ranks[dim - 1] < loads[a][dim - 2])?
            ranks[dim - 1]:
            loads[a][dim - 2]
        );
        ++r
    )
    {
        for (int i = 0; i < mchunk; ++i)
        {
            tmp[i + ((offs[a][dim - 2] + r) % ranks[dim - 1]) * mchunk]
                = carriages[dim - 1][
                    (offs[a][dim - 2] + r) % ranks[dim - 1]
                    + (moff + i) * ranks[dim - 1]
                ] * arg[i];
        }
    }

    for (int d = dim - 2; d >= 0; --d)
    {
        is_lchunk = (lchunks[a][d]? 1: 0);
        is_rchunk = (rchunks[a][d]? 1: 0);

        //=========================================//
        //    Next left carriage partial update    //
        //=========================================//
        // lucar[lr][rr] = carriages[d][lr][rr] x arg
#pragma omp parallel for collapse(2)
        for (int r = 0; r < loads[a][d]; ++r)
        {
            for (int i = 0; i < mchunk; ++i)
            {
                lucar[i + r * mchunk]
                    = carriages[d][r + (moff + i) * loads[a][d]] * arg[i];
            }
        }

        //========================================//
        //    Convolution along mode dimension    //
        //========================================//
        // conv[lr][rr] = lucar[lr][rr] * rucarc[rr]
#pragma omp parallel for
        for (int r = 0; r < loads[a][d]; ++r)
        {
            int addr
                = (r < lchunks[a][d] || r >= loads[a][d] - rchunks[a][d])? r:
                lchunks[a][d]
                + ((r - lchunks[a][d]) % ranks[d + 1]) * spans[a][d]
                + (r - lchunks[a][d]) / ranks[d + 1];

            MPI_Barrier(MPI_COMM_WORLD);
            this->conv(
                omp_get_thread_num(),
                lucar + r * mchunk,
                rucarc + ((offs[a][d] + r) % ranks[d + 1]) * mchunk,
                conv + addr * mchunk
            );
        }

        //=================================================//
        //    Sum of left carriage along mode dimension    //
        //=================================================//
        // aux = lucar · [1]
        if (loads[a][d])
        {
            gemv(
                CblasRowMajor, CblasNoTrans,
                loads[a][d], mchunk, dones[0],
                lucar, mchunk,
                dones, 1,
                dzero, aux, 1
            );
        }

        if (psize > 1)
        {
            MPI_Barrier(MPI_COMM_WORLD);
            MPI_Allreduce(
                MPI_IN_PLACE, aux, loads[a][d],
                MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD
            );
        }

        //==================================================//
        //    Partial product along right rank dimension    //
        //==================================================//
        // lucar = tmp · aux^T
        if (is_lchunk)
        {
            gemm(
                CblasColMajor, CblasNoTrans, CblasNoTrans,
                mchunk, 1, lchunks[a][d], dones[0],
                tmp + (offs[a][d] % ranks[d + 1]) * mchunk, mchunk,
                aux, lchunks[a][d],
                dzero, lucar, mchunk
            );
        }

        if (spans[a][d])
        {
            gemm(
                CblasColMajor, CblasNoTrans, CblasNoTrans,
                mchunk, spans[a][d], ranks[d + 1], dones[0],
                tmp, mchunk,
                aux + lchunks[a][d], ranks[d + 1],
                dzero, lucar + is_lchunk * mchunk, mchunk
            );
        }

        if (is_rchunk)
        {
            gemm(
                CblasColMajor, CblasNoTrans, CblasNoTrans,
                mchunk, 1, rchunks[a][d], dones[0],
                tmp, mchunk,
                aux + loads[a][d] - rchunks[a][d], rchunks[a][d],
                dzero, lucar + (is_lchunk + spans[a][d]) * mchunk, mchunk
            );
        }

        // rucarc = [0]
        // rucarp = [0]
        copy(2 * ranks[d] * mchunk, &dzero, 0, rucarc, 1); 

        // rucarp = lucar
        if (loads[a][d])
        {
            copy(
                (is_lchunk + spans[a][d] + is_rchunk) * mchunk,
                lucar, 1, rucarp + (offs[a][d] / ranks[d + 1]) * mchunk, 1
            );
        }

        //==============================================================//
        //    Partial sum of convolutions along right rank dimension    //
        //==============================================================//
        // rucarc = conv · [1]
        if (is_lchunk)
        {
            gemv(
                CblasColMajor, CblasNoTrans,
                mchunk, lchunks[a][d],
                dones[0], conv, mchunk,
                dones, 1,
                dzero, rucarc + (offs[a][d] / ranks[d + 1]) * mchunk, 1
            );
        }

        if (spans[a][d])
        {
            gemv(
                CblasColMajor, CblasNoTrans,
                spans[a][d] * mchunk, ranks[d + 1],
                dones[0], conv + lchunks[a][d] * mchunk, spans[a][d] * mchunk,
                dones, 1,
                dzero, rucarc
                    + ((offs[a][d] + lchunks[a][d]) / ranks[d + 1]) * mchunk, 1
            );
        }

        if (is_rchunk)
        {
            gemv(
                CblasColMajor, CblasNoTrans,
                mchunk, rchunks[a][d],
                dones[0],
                conv + (loads[a][d] - rchunks[a][d]) * mchunk, mchunk,
                dones, 1,
                dzero, rucarc + ((offs[a][d] + lchunks[a][d]) / ranks[d + 1]
                    + spans[a][d]) * mchunk, 1
            );
        }

        if (d)
        {
            tmp = rucarp;
            rucarp = rucarc + ranks[d - 1] * mchunk;
        }
    }

    //=======================//
    //    Result finalize    //
    //=======================//
    // Exchange of mode chunks boundaries 
    MPI_Request mpi_request;
    MPI_Status mpi_status;

    MPI_Barrier(MPI_COMM_WORLD);
    if (prank != psize - 1)
    {
        MPI_Isend(
            rucarc + (mchunk - dim + 1), dim - 1, MPI_DOUBLE,
            prank + 1, prank, MPI_COMM_WORLD, &mpi_request
        );
    }

    if (prank)
    {
        MPI_Recv(
            res, dim - 1, MPI_DOUBLE,
            prank - 1, prank - 1, MPI_COMM_WORLD, &mpi_status
        );
    }

    // res = 0
    if (!prank)
    {
        copy(dim - 1, &dzero, 0, res, 1); 
    }

    // res = rucarc >> dim - 1
    copy(mchunk - dim + 1, rucarc, 1, res + dim - 1, 1);

    // res -= (a + 2) * rucarp
    axpy(mchunk, -(double)((A == 1)? D: a + 2), rucarp, 1, res, 1);
    scal(mchunk, mult[a], res, 1);

    MPI_Barrier(MPI_COMM_WORLD);
    return;
}

//============================================================================//
//    Smoluchowski operator                                                   //
//============================================================================//
void TKernel_TT::compute(
    // in:
    const double * arg,
    // out: S(arg)
    double * res
) {
    double * tmp = aux + ((L + R) << 1) * mchunk + L;

    this->gsmol(0, arg, res);

    for (int a = 1; a < A; ++a)
    {
        this->gsmol(a, arg, tmp);
        axpy(mchunk, 1., tmp, 1, res, 1);
    }

    return;
}

//============================================================================//
//    Gather mode chunks                                                      //
//============================================================================//
void TKernel_TT::collect(
    // alt:
    double * arg
) {
    int * pmchunks = NULL; 
    int * pmoffs = NULL;

    if (!prank)
    {
        pmchunks = (int *)malloc(psize * sizeof(int)); 
        pmoffs = (int *)malloc(psize * sizeof(int));
    }

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Gather(
        &mchunk, 1, MPI_INT,
        pmchunks, 1, MPI_INT, 0, MPI_COMM_WORLD 
    );

    MPI_Gather(
        &moff, 1, MPI_INT,
        pmoffs, 1, MPI_INT, 0, MPI_COMM_WORLD 
    );

    if (!prank)
    {
        pmchunks[0] = 0;
    }

    MPI_Gatherv(
        (!prank)? MPI_IN_PLACE: arg, mchunk, MPI_DOUBLE,
        arg, pmchunks, pmoffs,
        MPI_DOUBLE, 0, MPI_COMM_WORLD
    );

    if (!prank)
    {
        free(pmchunks);
        free(pmoffs);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    return;
}

