#ifndef GSMOL_TT_KERNEL_H
#define GSMOL_TT_KERNEL_H

#include "mkl.h"
#include "mkl_lapacke.h"
#include "tensor_train.h"

//============================================================================//
//    Class for Generalized Smoluchowski operator                             //
//============================================================================//
class TKernel_TT
{

    private:

        // kernels maximal dimension
        int D;

        // quantity of kernels
        int A;

        // kernels mode size
        int N;

        // maximal rank
        int R;

        // multipliers in front of kernels
        // factorial coefficient * weight
        double * mult;

        // kernels in TT-format
        TTensorTrain ** kers_tt;

        // auxiliary memory
        double * aux;
        
        // convolution tasks
        VSLConvTaskPtr * tasks;

        // ranks of kernels after approximation
        int * ranks_appr;

        // evaluated time of approximation
        double t_appr;

        // evaluated time of compression
        double t_compress;

        //============================================//
        //    Income part of Smoluchowski operator    //
        //============================================//
        void gconv(
            // in: index of kernel to apply
            const int & kind,
            // in:
            const double * arg,
            // out: income part of S^(kind)(arg)
            double * res
        );

        //=============================================//
        //    Outcome part of Smoluchowski operator    //
        //=============================================//
        void gmatvec(
            // in: index of kernel to apply
            const int & kind,
            // in:
            const double * arg,
            // out: outcome part of S^(kind)(arg)
            double * res
        );

    public:

        TKernel_TT():
            D(-1),
            A(-1),
            N(-1),
            R(-1),
            mult(NULL),
            kers_tt(NULL),
            aux(NULL),
            tasks(NULL),
            ranks_appr(NULL),
            t_appr(-1),
            t_compress(-1)
        {}

        TKernel_TT(
            const char * kname,
            // in: type of collisions
            const char * ktype,
            // in: kernels maximal dimension
            const int & dim,
            // in: quantity of equations
            const int & mode,
            // in: weights in front of kernels
            const double * w,
            // in: tolerance of approximation
            const double & tol
        );

        virtual ~TKernel_TT();

        // return: evaluated time of approximation
        double get_t_appr() { return t_appr; }

        // return: evaluated time of compression
        double get_t_compress() { return t_compress; }

        // return: d-th rank of last kernel before compression
        int get_rank_appr(
            // in: index of kernel
            int & d
        ) { return ranks_appr[d]; }

        // return: d-th rank of last kernel 
        int get_rank(
            // in: index of kernel
            int & d
        ) { return (kers_tt[A - 1])->get_rank(d); }

        //=============================//
        //    Smoluchowski operator    //
        //=============================//
        void compute(
            // in:
            const double * arg,
            // out: S(arg)
            double * res
        );

};

#endif // GSMOL_TT_KERNEL_H

