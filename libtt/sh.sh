#!/bin/bash

#echo mpirun -n 5 ./gsmol_tt_mpi.out "inp/mpi_ibrae.inp" \
#    && mpirun -n 5 ./gsmol_tt_mpi.out "inp/mpi_ibrae.inp" \
#    && echo mv b b5 \
#    && mv b b5 \
#    &&
    echo mpirun -n 4 ./gsmol_tt_mpi.out "inp/mpi_ibrae.inp" \
    && mpirun -n 4 ./gsmol_tt_mpi.out "inp/mpi_ibrae.inp" \
    && echo mv b b4 \
    && mv b b4 \
    && echo mpirun -n 3 ./gsmol_tt_mpi.out "inp/mpi_ibrae.inp" \
    && mpirun -n 3 ./gsmol_tt_mpi.out "inp/mpi_ibrae.inp" \
    && echo mv b b3 \
    && mv b b3 \
    && echo mpirun -n 2 ./gsmol_tt_mpi.out "inp/mpi_ibrae.inp" \
    && mpirun -n 2 ./gsmol_tt_mpi.out "inp/mpi_ibrae.inp" \
    && echo mv b b2 \
    && mv b b2 \
    && echo mpirun -n 1 ./gsmol_tt_mpi.out "inp/mpi_ibrae.inp" \
    && mpirun -n 1 ./gsmol_tt_mpi.out "inp/mpi_ibrae.inp" \
    && echo mv b b1 \
    && mv b b1 \
    && vim -O bold b1 b2 b3 b4
