#ifndef GSMOL_TT_PARALLEL_KERNEL_H
#define GSMOL_TT_PARALLEL_KERNEL_H

#include "mpi.h"
#include "mkl.h"
#include "complex"
#include "mkl_cdft.h"
#include "mkl_lapacke.h"
#include "tensor_train.h"

//============================================================================//
//    Class for Generalized Smoluchowski operator                             //
//============================================================================//
class TKernel_TT
{

    private:

        //======================================//
        //    Fields containing kernels data    //
        //======================================//
        // kernels maximal dimension
        int D;

        // quantity of kernels
        int A;

        // kernels mode size
        int N;

        // maximal rank
        int R;

        // maximal load per processor
        int L;

        // multipliers in front of kernels
        // factorial coefficient * weight
        double * mult;

        // distributed kernels in TT-format
        TTensorTrain ** kers_tt;

        //============================================//
        //    Fields concerning MPI communications    //
        //============================================//
        // quantity of used processors
        int psize;

        // current processor rank
        int prank;

        // loads per processor for distributed TT-kernels
        int ** loads;

        // logical offsets for distributed TT-kernels
        int ** offs;
        
        // array of left chunks of loads
        int ** lchunks;

        // array of spans of loads
        int ** spans;

        // array of right chunks of loads
        int ** rchunks;

        // mode forward CDFT chunks
        MKL_LONG mchunk;

        // mode chunks leading dimensions
        MKL_LONG mld;

        // array of mode global offsets
        MKL_LONG moff;

        //============================================//
        //    Fields concerning OMP communications    //
        //============================================//
        // quantity of descriptors
        int H;
        
        // communicators for CDFT
        MPI_Comm * comms;

        // convolution descriptors
        DFTI_DESCRIPTOR_DM_HANDLE * descs;

        //===============================//
        //    Memory for calculations    //
        //===============================//
        // array of ones
        double * dones;

        // auxiliary memory for calculations
        double * aux;

        // auxiliary memory for DFT
        std::complex<double> * faux;

        //====================//
        //    Stats fields    //
        //====================//
        // ranks of kernels after approximation
        int * ranks_appr;

        // evaluated time of approximation
        double t_appr;

        // evaluated time of compression
        double t_compress;

        // evaluated time of transfers
        double t_transfer;

        //====================================================//
        //    Binary convolution using +1 and -1 circulant    //
        //====================================================//
        void conv(
            // in: index of CDFT descriptor
            const int & h,
            // in: arg1
            const double * arg1,
            // in: arg2
            const double * arg2,
            // out: arg1 * arg2
            double * res
        );

        //============================================//
        //    Income part of Smoluchowski operator    //
        //============================================//
        void gconv(
            // in: index of kernel to apply
            const int & a,
            // in:
            const double * arg,
            // out: income part of S^(a)(arg)
            double * res
        );

        //=============================================//
        //    Outcome part of Smoluchowski operator    //
        //=============================================//
        void gmatvec(
            // in: index of kernel to apply
            const int & a,
            // in: 
            const double * arg,
            // out: outcome part of S^(a)(arg)
            double * res
        );

        //=============================================//
        //    Smoluchowski operator for a-th kernel    //
        //=============================================//
        void gsmol(
            // in: index of kernel to apply
            const int & a,
            // in: 
            const double * arg,
            // out: S^(a)(arg)
            double * res
        );

    public:

        TKernel_TT(
            void
        );

        TKernel_TT(
            const char * kname,
            // in: type of collisions
            const char * ktype,
            // in: kernels maximal dimension
            const int & kdim,
            // in: quantity of equations
            const int & kmode,
            // in: weights in front of kernels
            const double * kw,
            // in: tolerance of approximation
            const double & ktol,
            // in: MPI thread support
            const int & support
        );

        virtual ~TKernel_TT(
            void
        );

        // return: evaluated time of approximation
        double get_t_appr(
            void
        ) { return t_appr; }

        // return: evaluated time of compression
        double get_t_compress(
            void
        ) { return t_compress; }

        // return: evaluated time of transfer
        double get_t_transfer(
            void
        ) { return t_transfer; }

        // return: d-th approximation rank of the highest kernel 
        int get_rank_appr(
            // in: index of rank
            int & d
        ) { return ranks_appr[d]; }

        // return: d-th rank of the highest kernel 
        int get_rank(
            // in: index of rank
            int & d
        ) { return (kers_tt[A - 1])->get_rank(d); }

        int get_mchunk(
            void
        ) { return mchunk; }

        //=============================//
        //    Smoluchowski operator    //
        //=============================//
        void compute(
            // in:
            const double * arg,
            // out: S(arg)
            double * res
        );

        //==========================//
        //    Gather mode chunks    //
        //==========================//
        void collect(
            // alt:
            double * arg
        );
};

#endif // GSMOL_TT_PARALLEL_KERNEL_H

