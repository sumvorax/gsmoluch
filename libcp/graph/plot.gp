set terminal eps

#set output 'num_an_solution.eps'
#set xlabel 'размер k'
#set ylabel 'концентрация n_{k}'
# set xrange [0:50.6]
# set yrange [0:0.03]
# #set logscale x
# #set format y '10^{%L}'
# #set format x '10^{%L}'
# 
# plot 'anal_solution' using ($1 + 1):2 w p pt 2 lt rgb "black" t 'аналитическое решение', \
#      'my_solution' using ($1 + 1):2 w p pt 6 lt rgb "black" t 'численное решение', \
#      'my_solution' using ($1 + 1):2 w i dt 2 lt rgb "black" t '', \

## set output 'const_evolution.eps'
## set autoscale x
## set autoscale y
## set logscale x
## set logscale y
## set yrange [1e-14:1e-0]
## set format y '10^{%L}'
## set format x '10^{%L}'
## 
## plot '../results/l_const23_131072_0.01_100' using 1:2 w l lw 3 lt rgb "black" dt 1 t 't = 1', \
##      '../results/l_const23_131072_0.01_1000' using 1:2 w l lw 3 lt rgb "black" dt "_" t 't = 10', \
##      '../results/l_const23_131072_0.01_10000' using 1:2 w l lw 3 lt rgb "black" dt "--" t 't = 10^{2}', \
##      '../results/l_const23_131072_0.01_100000' using 1:2 w l lw 3 lt rgb "black" dt "-." t 't = 10^{3}', \
##      '../results/l_const23_131072_0.01_1000000' using 1:2 w l lw 3 lt rgb "black" dt ".." t 't = 10^{4}', \

### set output 'const_vs_genprod.eps'
### set yrange [1e-14:1e-1]
### plot '../results/s_const23_262144_25_0.01_2500' using 1:2 w l lw 2 lt rgb "black" dt 1 t 'I тип ядер, t = 25', \
###      '../results/s_const23_262144_50_0.01_5000' using 1:2 w l lw 2 lt rgb "black" dt "_" t 'I тип ядер, t = 50', \
###      '../results/s_const23_262144_75_0.01_7500' using 1:2 w l lw 2 lt rgb "black" dt "-" t 'I тип ядер, t = 75', \
###      '../results/s_genprod23_262144_25_0.01_2500' using 1:2 w l lw 2 lt rgb "black" dt "_._.. " t 'II тип ядер, t = 25', \
###      '../results/s_genprod23_262144_50_0.01_5000' using 1:2 w l lw 2 lt rgb "black" dt "-.." t 'II тип ядер, t = 50', \
###      '../results/s_genprod23_262144_75_0.01_7500' using 1:2 w l lw 2 lt rgb "black" dt "." t 'II тип ядер, t = 75', \

#<># set output 'const3.eps'
#<># set logscale x
#<># set logscale y
#<># set yrange [1e-14:1e-1]
#<># set xrange [1e1:1e4]
#<># set format y '10^{%L}'
#<># set format x '10^{%L}'
#<># plot '../results/s_const_2-all_131072_100_0.01_10000' using 1:2 w l lw 2 lt rgb "black" dt 1 t 'd = 2', \
#<>#      '../results/s_const_3-only_131072_100_0.01_10000' using 1:2 w i lw 2 lt rgb "black" dt "_" t 'd = 3', \

#<># set output 'const4.eps'
#<># set logscale x
#<># set logscale y
#<># set yrange [1e-14:1e-1]
#<># set xrange [1e1:1e4]
#<># set format y '10^{%L}'
#<># set format x '10^{%L}'
#<># plot '../results/s_const_2-all_131072_100_0.01_10000' using 1:2 w l lw 2 lt rgb "black" dt 1 t 'd = 2', \
#<>#      '../results/s_const_4-only_131072_100_0.01_10000' using 1:2 w i lw 2 lt rgb "black" dt "-" t 'd = 4', \

set output 'genprod_d-time.eps'
set key top left
set xlabel 'Размерность D'
set ylabel 'Время вычисления (сек)'
#set autoscale x
#set autoscale y
#set logscale y
set yrange [-100:1400]
set xrange [1.8:5.2]
set xtics 1
#set format y '10^{%L}'
#set format x '10^{%L}'
plot '~/Latex/articles/mm_smoluch/genprod_d-time-14' using 1:2 w lp pt 4 lw 3 lt rgb "black" dt 1 t 'N = 2^{14}', \
     '~/Latex/articles/mm_smoluch/genprod_d-time-15' using 1:2 w lp pt 6 lw 3 lt rgb "black" dt "_" t 'N = 2^{15}', \
     '~/Latex/articles/mm_smoluch/genprod_d-time-16' using 1:2 w lp pt 8 lw 3 lt rgb "black" dt "-" t 'N = 2^{16}', \
     '~/Latex/articles/mm_smoluch/genprod_d-time-17' using 1:2 w lp pt 12 lw 3 lt rgb "black" dt "-." t 'N = 2^{17}', \

set output 'gensum_d-rank.eps'
set key top left
set xlabel 'Размерность D'
set ylabel 'Максимальный TT-ранг R'
#set autoscale x
#set autoscale y
#set logscale y
set yrange [10:22]
set xrange [1.8:5.2]
set xtics 1
#set format y '10^{%L}'
#set format x '10^{%L}'
plot '~/Latex/articles/mm_smoluch/gensum_d-rank-5' using 1:2 w lp pt 4 lw 3 lt rgb "black" dt 1 t 'ε = 10^{-5}', \
     '~/Latex/articles/mm_smoluch/gensum_d-rank-6' using 1:2 w lp pt 8 lw 3 lt rgb "black" dt "_" t 'ε = 10^{-6}', \
     '~/Latex/articles/mm_smoluch/gensum_d-rank-7' using 1:2 w lp pt 12 lw 3 lt rgb "black" dt "-" t 'ε = 10^{-7}', \
