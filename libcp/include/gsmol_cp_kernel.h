#ifndef GSMOL_CP_KERNEL_H
#define GSMOL_CP_KERNEL_H

#include<complex>
#include<mkl_dfti.h>

//============================================================================//
//    Kernel class                                                            //
//============================================================================//
class TKernel_CP
{
    private:

        // kernels maximal dimension 
        int D;

        // kernels mode size 
        int N;

        // kernels ranks
        int * R;

        // multipliers in front of kernels
        // factorial coefficient * weight
        double * mult;

        // kernels CPD data
        double * kdata;

        // quantities of significant arguments
        int * asgn;

        // presence of trivial argument
        int * intrarg;

        // kernel-update auxiliary memory
        double * updaux;

        // FFT auxiliary memory
        std::complex<double> * fftaux;

        // FFT descriptor
        DFTI_DESCRIPTOR_HANDLE ffthand;

        //==================================================//
        //    Smoluchowski updates of argument by kernel    //
        //    for r-th convolution                          //
        //==================================================//
        //  alt: updaux[] = kernel-updates
        //  of argument by kdata
        void update(
            // in: index of convolution summand to handle
            const int & r,
            // in: argument to update
            const double * arg,
            // out: arity of convolution
            int & d,
            // out: moments of updated arguments
            double * w
        );

    public:

        TKernel_CP();

        TKernel_CP(
            // in: kernel name
            const char * kname,
            // in: collisions types
            const char * ktype,
            // in: kernels maximal dimension
            const int & dim,
            // in: number of equations
            const int & mode,
            // in: weights of kernels
            const double * alpha
        );

        virtual ~TKernel_CP();

        //=============================//
        //    Smoluchowski operator    //
        //=============================//
        void compute(
            // in:
            const double * arg,
            // out: S(arg)
            double * res
        );

};

#endif // GSMOL_CP_KERNEL_H
