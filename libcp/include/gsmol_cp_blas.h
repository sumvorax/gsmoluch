#ifndef GSMOL_CP_BLAS_H
#define GSMOL_CP_BLAS_H

#include<complex>
#include<omp.h>
#include"mkl.h"
#include"mkl_lapacke.h"

//============================================================================//
//    BLAS wrappers                                                           //
//============================================================================//
void scal(
    const int & N,
    const double & da,
    double * dx,
    const int & incx
);

void copy(
    const int & N,
    const double * dx,
    const int & incx,
    double * dy,
    const int & incy
);

void copy(
    const int & N,
    const int * ix,
    const int & incx,
    int * iy,
    const int & incy
);

void axpy(
    const int & N,
    const double & da,
    const double * dx,
    const int & incx,
    double * dy,
    const int & incy
);

double dot(
    const int & N,
    const int * ix,
    const int & incx,
    const int * iy,
    const int & incy
);

void gemv(
    const CBLAS_LAYOUT & Layout,
    const CBLAS_TRANSPOSE & trans,
    const int & m,
    const int & n,
    const double & alpha,
    const double * a,
    const int & lda,
    const double * x,
    const int & incx,
    const double & beta,
    double * y,
    const int & incy
);

#endif // GSMOL_CP_BLAS_H

