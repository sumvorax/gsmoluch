#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<ctime>
#include<complex>

#include"../include/gsmol_cp_blas.h"
#include"../include/gsmol_cp_utility.h"

void load_from_file(
    const char * filename,
    const int & N,
    double * n
) {
    FILE * f;
    int tmp;

    f = fopen(filename, "rb");

    fread(&tmp, sizeof(int), 1, f);

    for (int i = 0; i < N; ++i)
    {
        fread(n + i, sizeof(double), 1, f);
    }

    fclose(f);
}

//============================================================================//
//    Main                                                                    //
//============================================================================//
int main(
    int argc,
    char ** argv
) {
    if (argc < 3)
    {
        printf(
            "================================================================================\n"
        );

        printf(
            "Required arguments:\n (1) filename_from\n (2) filename_to\n (3) N\n"
        );


        printf(
            "================================================================================\n"
        );

        return 1;
    }

    int N;
    sscanf(argv[3], "%d", &N);

    double * n = (double *)malloc(N * sizeof(double));
    load_from_file(
        argv[1], N, n
    );

    FILE * fres = fopen(argv[2], "w");

    for (int i = 0; i < N; ++i)
        fprintf(fres, "%d %.15f\n", i, n[i]);

    fclose(fres);

    //=========================//
    //    Deallocate memory    //
    //=========================//
    free(n);

    return 0;
}
 
