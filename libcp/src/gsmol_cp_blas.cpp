#include<complex.h>

#include"../include/gsmol_cp_blas.h"

//============================================================================//
//    BLAS wrappers                                                           //
//============================================================================//
void scal(
    const int & N,
    const double & da,
    double * dx,
    const int & incx
) {
    cblas_dscal(N, da, dx, incx);

    return;
};

void copy(
    const int & N,
    const double * dx,
    const int & incx,
    double * dy,
    const int & incy
) {
    cblas_dcopy(N, dx, incx, dy, incy);

    return;
};

void copy(
    const int & N,
    const int * ix,
    const int & incx,
    int * iy,
    const int & incy
) {
#pragma omp parallel for
    for (int i = 0; i < N; ++i)
        iy[i * incy] = ix[i * incx];

    return;
}

void axpy(
    const int & N,
    const double & da,
    const double * dx,
    const int & incx,
    double * dy,
    const int & incy
) {
    cblas_daxpy(N, da, dx, incx, dy, incy);

    return;
};

double dot(
    const int & N,
    const int * ix,
    const int & incx,
    const int * iy,
    const int & incy
) {
    int res = 0;

#pragma omp parallel for reduction(+:res)
    for (int i = 0; i < N; ++i)
        res += ix[i * incx] * iy[i * incy];

    return res;
}

void gemv(
    const CBLAS_LAYOUT & Layout,
    const CBLAS_TRANSPOSE & trans,
    const int & m,
    const int & n,
    const double & alpha,
    const double * a,
    const int & lda,
    const double * x,
    const int & incx,
    const double & beta,
    double * y,
    const int & incy
) {
    cblas_dgemv(Layout, trans, m, n, alpha, a, lda, x, incx, beta, y, incy);

    return;
}
