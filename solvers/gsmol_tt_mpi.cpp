#include <stdio.h>
#include <cstdlib>
#include <mpi.h>
#include <mkl.h>
#include <cmath>
#include <cstring>
#include <omp.h>
#include "../libtt/parallel_cross_omp.h"
#include "../libtt/tensor_train.h"
#include "../libtt/gsmol_tt_parallel_kernel.h"
#include <unistd.h>

using namespace std;

void save_to_file(
    const char * filename,
    const int & N,
    const double * n,
    const int & k
) {
    FILE * f;

    f = fopen(filename, "wb");

    fwrite(&k, sizeof(int), 1, f);

    for (int i = 0; i < N; ++i)
    {
        fwrite(n + i, sizeof(double), 1, f);
    }

    fclose(f);
}

void load_from_file(
    const char * filename,
    const int & N,
    double * n
) {
    FILE * f;
    int tmp;

    f = fopen(filename, "rb");

    fread(&tmp, sizeof(int), 1, f);

    for (int i = 0; i < N; ++i)
    {
        fread(n + i, sizeof(double), 1, f);
    }

    fclose(f);
}

//============================================================================//
//    Main                                                                    //
//============================================================================//
int main(int argc, char ** argv)
{
    int mpi_thread_support;
    MPI_Init_thread(
        &argc, &argv, MPI_THREAD_MULTIPLE, &mpi_thread_support
    );

    int psize;
    int prank;

    MPI_Comm_size(MPI_COMM_WORLD, &psize);
    MPI_Comm_rank(MPI_COMM_WORLD, &prank);

    if (argc < 2)
    {
        if (!prank) 
        {
            printf(
                "=============================================================="
                "==================\n"
            );

            printf(
                "Required arguments:\n"
                "(1) kernel_name\n"
                "(2) collisions_type\n"
                "(3) dimensionality\n"
                "(4) weights in front of kernels\n"
                "(5) mode_size\n"
                "(6) number_iterations\n"
                "(7) dt\n"
                "(8) tolerance\n"
                "(9) filename_time\n"
                "(10) filename_result\n"
                "(11) filename_ranks\n"
                "(12) filename_temporary\n"
                "(13) filename_moments_result\n"
            );

            printf(
                "=============================================================="
                "==================\n"
            );

            printf(
                "Tips: kernel_name"
                "== {const, sumsqrt, genprod, gensum, ibrae}\n"
                "Tips: kernel_name = const, otherwise\n"
                "Tips: ibrae == const, if dimensionality > 3\n"
            );

            printf(
                "=============================================================="
                "==================\n"
            );

            printf(
                "Tips: collisions_type"
                "== all   --   all collisions till dimensionality\n"
                "Tips: collisions_type"
                "!= all   --   only dimensionality-ary collisions\n"
            );

            printf(
                "=============================================================="
                "==================\n"
            );

            printf(
                "Tips: './log' file to be appended if present,"
                "to be generated otherwise\n"
            );

            printf(
                "=============================================================="
                "==================\n"
            );
        }

        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    int status;

    char kname[256];
    char ktype[256];
    int D;
    int A;
    double * w;
    int N;
    int T;
    double dt;
    double tol;
    char ft_name[256];
    char fres_name[256];
    char frk_name[256];
    char ftmp_name[256];
    char fmres_name[256];

    // read parameters from input file on root
    if (!prank)
    {
        FILE * in = fopen(argv[1], "r");
        
        status = fscanf(in, "%s\n", kname);
        status = fscanf(in, "%s\n", ktype);
        status = fscanf(in, "%d\n", &D);
        A = (!strcmp(ktype, "all"))? D - 1: 1;

        w = (double *)malloc(A * sizeof(double));

        for (int a = 0; a < A; ++a)
        {
            status = fscanf(in, "%lf", w + a);
        }
        status = fscanf(in, "\n");

        status = fscanf(in, "%d\n", &N);
        status = fscanf(in, "%d\n", &T);

        if (N < 1 || D < 2 || T < 1)
        {
            free(w);

            MPI_Abort(MPI_COMM_WORLD, 2);
        }

        status = fscanf(in, "%lf\n", &dt);
        status = fscanf(in, "%lf\n", &tol);
        status = fscanf(in, "%s\n", ft_name);
        status = fscanf(in, "%s\n", fres_name);
        status = fscanf(in, "%s\n", frk_name);
        status = fscanf(in, "%s\n", ftmp_name);
        status = fscanf(in, "%s\n", fmres_name);

        fclose(in);
    }

    // broadcast parameters
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(kname, 256, MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(ktype, 256, MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(&D, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&A, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&N, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&T, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dt, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&tol, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);
    if (prank)
    {
        w = (double *)malloc(A * sizeof(double));
    }

    MPI_Bcast(w, A, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    // initialize kernel
    MPI_Barrier(MPI_COMM_WORLD);
    TKernel_TT * kernel = new TKernel_TT(
        kname, ktype, D, N, w, tol, mpi_thread_support
    );

    free(w);

    const int mchunk = kernel->get_mchunk();

    // auxiliary memory allocation
    double * u = (double *)malloc(N * sizeof(double));
    double * v = (double *)malloc(N * sizeof(double));

    //======================================//
    //    Monodisperse initial condition    //
    //======================================//
    double * n = (double *)calloc(N, sizeof(double));
    if (!prank)
    {
        n[0] = 1.;
    }

    double m[3] = {1., 1., 1.};

    FILE * g = fopen(fmres_name, "w");
    fprintf(g, "%d: %.14f %.14f %.14f\n", 0, m[0], m[1] / m[0], m[2] / m[0]);
    fclose(g);

    MPI_Barrier(MPI_COMM_WORLD);
    //===========================//
    //    Predictor-corrector    //
    //===========================//
    double t_scheme = MPI_Wtime();

    for (int k = 0; k < T; ++k) 
    {
        kernel->compute(n, u);

        // u *= 0.5 * dt
        scal(N, 0.5 * dt, u, 1);
        // u += n
        axpy(N, 1., n, 1, u, 1);

        kernel->compute(u, v);

        // n += dt * v
        axpy(N, dt, v, 1, n, 1);

        /*for (int i = 0; i < N; i++)
        {
            if (n[i] < 0.0)
                n[i] = 0.0;
        }*/
        if (k % 100 == 99)
        {
            double m_new[3] = {0., 0., 0.};

            for (int i = 0; i < N; ++i)
            {
                m_new[0] += n[i];
                m_new[1] += (double)(i + 1) * n[i];
                m_new[2] += pow((double)(i + 1), 3.) * n[i];
            }

            printf("%.15f %.15f\n", m_new[1], m[1]);

            // if ((std::isnan(m_new[1]) || (abs(m_new[1] - 1.) > 1e-10)))
            // if (isnan(m_new[1]) || (m_new[1] != 1.))
            // if ((std::isnan(m_new[1])))// || (abs(n[N - 1]) > 1e-14)))
            if (
                (std::isnan(m_new[1]))
                || ((abs(n[N / 3 - 1]) > 1e-14) && (N < 131072))
            )
            {
                k -= 100;

                n = (double *)realloc(n, (N << 1) * sizeof(double));
                u = (double *)realloc(u, (N << 1) * sizeof(double));
                v = (double *)realloc(v, (N << 1) * sizeof(double));

                if (k > 0)
                {
                    load_from_file(ftmp_name, N, n);
                }
                else
                {
                    n[0] = 0.;
                    copy(N, n, 0, n, 1);
                    n[0] = 1.;
                }

                n[N] = 0.;
                copy(N, n + N, 0, n + N, 1);

                N <<= 1;

                delete kernel;

                kernel = new TKernel_TT(
                    kname, ktype, D, N, w, tol, mpi_thread_support
                );
            }
            else
            {
                copy(3, m_new, 1, m, 1);

                g = fopen(fmres_name, "a");
                fprintf(g, "%d: %.14f %.14f %.14f\n", k, m[0], m[1] / m[0], m[2] / m[0]);
                fclose(g);

                save_to_file(ftmp_name, N, n, k);
            }
        }
    }
    
    t_scheme = MPI_Wtime() - t_scheme;

    double t_appr = kernel->get_t_appr();
    double t_compress = kernel->get_t_compress();
    double t_overall = t_appr + t_compress + t_scheme;
    double t_transfer = kernel->get_t_transfer();

    kernel->collect(n);

    if (!prank)
    {
        //==================================//
        //    Write times & ranks >> log    //
        //==================================//
        FILE * f = fopen("log", "a");

        fprintf(
            f,
            "\n%d %s %d-%s %d\ntol: %.8f\nt: %d %.8f %d\ntimes: %.2f %.2f %.2f "
            "%.2f %.2f\nranks_appr: ", psize,
            kname, D, (strcmp(ktype, "all"))? "only": "all", N, tol,
            (int)rint(dt * T), dt, T, t_appr, t_compress, t_transfer, t_scheme,
            t_overall
        );

        for (int d = 1; d < D; ++d)
        {
            fprintf(f, "%d ", kernel->get_rank_appr(d));
        }

        fprintf(f, "\nranks_compress: ");

        for (int d = 1; d < D; ++d)
        {
            fprintf(f, "%d ", kernel->get_rank(d));
        }

        fprintf(f, "\n");

        fclose(f);

        //=============================//
        //    Write times > ft_name    //
        //=============================//
        f = fopen(ft_name, "a");

        fprintf(f, "\n%d-%s %d tol: %.8f\ntimes: %.2f %.2f %.2f %.2f\n", D,
            (strcmp(ktype, "all"))? "only": "all", N, tol,
            t_appr, t_compress, t_scheme, t_overall
        );

        fclose(f);

        //=================================//
        //    Write results > fres_name    //
        //=================================//
        f = fopen(fres_name, "w");

        for (int i = 0; i < N; ++i)
        {
            fprintf(f, "%d %.15f\n", i, n[i]);
        }

        fclose(f);

        //==============================//
        //    Write ranks > frk_name    //
        //==============================//
        f = fopen(frk_name, "w");

        fprintf(
            f, "\ns %s %d-%s %d\ntol: %.8f\nt: %d %.8f %d\nranks_appr: ",
            kname, D, (strcmp(ktype, "all"))? "only": "all", N, tol,
            (int)rint(dt * T), dt, T
        );

        for (int d = 1; d < D; ++d)
        {
            fprintf(f, "%d ", kernel->get_rank_appr(d));
        }

        fprintf(f, "\nranks_compress: ");

        for (int d = 1; d < D; ++d)
        {
            fprintf(f, "%d ", kernel->get_rank(d));
        }

        fprintf(f, "\n");

        fclose(f);
    }

    //=========================//
    //    Deallocate memory    //
    //=========================//
    delete kernel;

    free(n);
    free(u);
    free(v);

    MPI_Finalize();
    return 0;
}
